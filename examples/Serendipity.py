import numpy as np
import fetk.element
import fetk.material
from fetk.fe_model import fe_model


def example():
    nodes = [
        [1, 1, 0],
        [2, 2, 0],
        [3, 3, 0],
        [4, 0.5, 2],
        [5, 3.5, 2],
        [6, 0, 4],
        [7, 2, 4],
        [8, 4, 4],
    ]
    elements = [[1, 1, 3, 8, 6, 2, 5, 7, 4]]
    model = fe_model("Job-1", nodes, elements)

    E, Nu, rho = 30e9, 0.2, 2400
    properties = {"thickness": 1.0}

    element = fetk.element.C2D8E(**properties)
    material = fetk.material.elastic({"E": E, "nu": Nu}, density=rho)
    model.element_block("Block-1", elements=[1], element=element, material=material)

    model.nodeset("Nodeset-1", nodes=(6, 7, 8))
    model.boundary("Nodeset-1", "xy", 0)

    t = np.array([400e3, -300e3])
    sides = model.find_sides2d(lambda x: x[:, 1] == 0)
    model.sideset("Sideset-1", sides=sides)
    model.surface_force("Sideset-1", t)

    g = np.array([0, -9.81])
    model.element_set("Elemset-1", elements=(1,))
    model.gravity("Elemset-1", g)

    model.solve()

    # Abaqus solution
    ua = [
        [0.000246637, -0.00010368],
        [0.00024365, -3.37288e-05],
        [0.000247235, 4.5522e-05],
        [8.17473e-05, -7.31956e-05],
        [7.50289e-05, 4.7124e-05],
        [0.0, 0.0],
        [0.0, 0.0],
        [0.0, 0.0],
    ]
    assert np.allclose(model.solution.u, ua)


if __name__ == "__main__":
    example()
