import numpy as np
import fetk.fe_model
import fetk.element
import fetk.material


def source(xc):
    x, y = xc[:2]
    return 20.0 * (x - 4) ** 2 * (y - 4) ** 2


def example():

    nodes = [[1, 0, 0], [2, 4, 0], [3, 0, 4], [4, 2, 2], [5, 4, 1]]
    elements = [[1, 1, 4, 3], [2, 1, 2, 5, 4]]
    model = fetk.fe_model.fe_model("Heat", nodes, elements)

    mat = fetk.material.factory("thermal", {"k": 10})

    el1 = fetk.element.DC2D3(thickness=1)
    model.element_block("Block-1", elements=[1], element=el1, material=mat)

    el2 = fetk.element.DC2D4(thickness=1)
    model.element_block("Block-2", elements=[2], element=el2, material=mat)

    model.sideset("Sideset-1", sides=[(1, 3)])
    model.surface_film("Sideset-1", 20, 100)

    model.element_set("Elset-1", elements=(1, 2))
    model.heat_source("Elset-1", source)

    model.nodeset("Nodeset-1", nodes=(2, 5))
    model.temperature("Nodeset-1", 100)

    model.sideset("Sideset-2", sides=[(2, 1)])
    model.surface_flux("Sideset-2", [0, 3000])

    model.solve()

    expected = [122.563, 100.0, -16.8469, 187.433, 100.0]
    assert np.allclose(model.solution.u[:, 0], expected, rtol=1e-5)


if __name__ == "__main__":
    example()
