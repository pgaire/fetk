================
Input file usage
================

Input file keyword: ``element blocks``

This option is used to define a *list* of element blocks.

-----------------------
Defining element blocks
-----------------------

Each element block in the list of element blocks contains:

* (Optional) The block name
* The material id
* The element type
* The element fabrication properties
* List of elements in the block

Repeat this entry for every element block in the model.

-------
Example
-------

.. code-block:: yaml

    element blocks:
    - block:
      name: block
      material: 1
      element type: L1D2
      element properties:
        area: 1.0
      elements: 1, 2, 3, 4
