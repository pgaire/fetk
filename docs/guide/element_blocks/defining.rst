=======================
Defining element blocks
=======================

Element blocks are defined in a mapping containing the element IDs of the
element block, material id, and fabrication properties of elements in
the block.  An optional descriptive name can also be given.

------------------------------------
Definining individual element blocks
------------------------------------

Individual element blocks are given as a mapping defining the elements in the block, the element fabrication properties, and element material properties:

.. code-block:: python

   element_block = {
       "name": block_name,
       "material": material_id,
       "elements": elements,
       "element type": element_type,
       "element properties": element_properties
   }

where ``block_name`` is an (optional) block name, ``material_id`` is the integer
ID for the material (see :ref:`materials <mat_index>`), ``elements`` is a list
of the external element IDs for the elements defining the block,
``element_type`` is the element type, and ``element_properties`` is a mapping of
element fabrication properties.

.. rubric:: Example 1

.. _fig_elem_block_defn:

.. figure:: ./blocks.png
   :width: 6in

   Mesh for element block definition example.

Consider the mesh shown in Figure :ref:`fig_elem_block_defn`.  The element
blocks defining the mesh are:

.. code-block:: python

   element_blocks = [
       {
           "name": "top longerons",
           "material": 1,
           "element type": "L2D2",
           "element properties": {"area": 10.00},
           "elements": [7, 8, 9, 10, 11, 12],
       },
       {
           "name": "battens",
           "material": 1,
           "element type": "L2D2",
           "element properties": {"area": 3.00},
           "elements": [13, 14, 15, 16, 17],
       },
       {
           "name": "diagonals",
           "material": 1,
           "element type": "L2D2",
           "element properties": {"area": 1.00},
           "elements": [18, 19, 20, 21],
       },
       {
           "name": "bottom longerons",
           "material": 1,
           "element type": "L2D2",
           "element properties": {"area": 2.00},
           "elements": [1, 2, 3, 4, 5, 6],
       },
   ]
