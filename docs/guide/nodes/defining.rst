.. _defining_nodes:

==============
Defining nodes
==============

Nodes are defined in a table giving the location of each node through the
coordinates stored in the table.  The table contains ``num_node`` sublist items:

.. code-block:: python

   nodes = [node_0, node_1, . . ., node_n]

Each of the components of ``nodes`` is a list defining the external node ID and coordinates of a node.  The node definition of internal node ``n`` is the nth item in the ``nodes`` table.

---------------------------
Definining individual nodes
---------------------------

Individual nodes are defined as a list containing 2-4 entries:

.. code-block:: python

   node = [nx, xn]

or

.. code-block:: python

   node = [nx, xn, yn]

or

.. code-block:: python

   node = [nx, xn, yn, zn]

where ``nx`` is the external node number for the internal node ``n``; and ``xn``, ``yn``, and ``zn`` are the homogeneous position coordinates of internal node ``n`` in the :math:`x`, :math:`y`, and :math:`z` coordinate directions, respectively.

.. rubric:: Example

.. _fig_node_defn:

.. figure:: ./node_defn.png
   :width: 4in

   Mesh for node definition example.

Consider the 9-node regular 2D mesh shown in Figure :ref:`fig_node_defn`.  The
node table is given by

.. code-block:: python

   nodes = [
       [1, 12., 12., 0.],
       [2, 12., 6., 0.],
       [3, 12., 0., 0.],
       [4, 18., 12., 0.],
       [5, 18., 6., 0.],
       [6, 18., 0., 0.],
       [7, 24., 12., 0.],
       [8, 24., 6., 0.],
       [9, 24., 0., 0.],
   ]
