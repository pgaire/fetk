.. _defining_bc:

============================
Defining boundary conditions
============================

Boundary conditions are defined in a table giving containing ``num_bc`` sublist items:

.. code-block:: python

   boundary = [boundary_0, boundary_1, . . ., boundary_n]

Each of the components of ``boundary`` is a list defining the boundary condition.

--------------------------------
Definining individual conditions
--------------------------------

Individual boundary conditions are defined as a list containing

.. code-block:: python

   bc = [nx, type, dof, magnitude]

where ``nx`` is the external node number, type is the boundary condition type (0 for Neumann, 1 for Dirichlet), ``dof`` is the coordinate direction of the condition (0 for :math:`x`, 1 for :math:`y`, and 2 for :math:`z`), and ``magnitude`` is the magnitude of the condition.

.. rubric:: Example


.. _fig_bc1:

.. figure:: ./bc1.png
   :width: 4in

   Support conditions and applied forces

The boundary conditions for the above structure are

.. code-block:: python

   neumann, dirichlet = 0, 1
   x, y = 0, 1
   boundary = [
      [1, dirichlet, y, 0],
      [2, neumann, y, -7200],
      [3, dirichlet, y, 0],
   ]
