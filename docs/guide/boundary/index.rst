===================
Boundary Conditions
===================

Boundary conditions at nodes are used to prescribe the value of the primary variable.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   description
   defining
   internal
   input
