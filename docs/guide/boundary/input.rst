================
Input file usage
================

----------------------------
Defining boundary conditions
----------------------------

Input file keyword: ``boundary``.

This option is used to define boundary conditions to nodes directly.

Each item in the list of boundary conditions contains:

1. The external node number
2. The degree of freedom constrained (x, y, z, xy, xz, y, yz)
3. The magnitude of the constraint

Repeat this entry for every boundary condition in the model.

-------
Example
-------

.. code-block:: yaml

    boundary:
    - 1, x, 0.
    - 1, y, 0.
    - 2, xy, 0.
    - 3, y, 5.


---------------------------
Defining concentrated loads
---------------------------

Input file keyword: ``cload``.

This option is used to define concentrated load conditions to nodes directly.

Each item in the list of boundary conditions contains:

1. The external node number
2. The degree of freedom constrained (x, y, z, xy, xz, y, yz)
3. The magnitude of the constraint

Repeat this entry for every concentrated load in the model.

-------
Example
-------

.. code-block:: yaml

    boundary:
    - 1, x, 7200
    - 1, y, 500
    - 2, xy, 1000
