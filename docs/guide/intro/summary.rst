=======
Summary
=======

This report presents a complete implementation of the finite element method
(FEM) using Python. The report focuses on data structures, data flow and
programming procedures for linear problems.

The present implementation has been designed with the following extensions in mind:

* Parallel computation
* Dynamic analysis, both transient and modal
* Nonlinear analysis
* Multiphysics problems
