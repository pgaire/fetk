=================
Distributed laods
=================

Distributed loads on elements are used to define distributed loads and point sources.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   description
   defining
   internal
   input
