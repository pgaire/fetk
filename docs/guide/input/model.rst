=============
The fem model
=============

A ``fetk`` model is composed of several different components that together
describe the physical problem to be analyzed and the results to be obtained. At
a minimum the model consists of the following information: geometry, element
blocks, material data, and loads and boundary conditions.

--------
Geometry
--------

Elements and nodes define the geometry of the physical structure being modeled
in ``fetk``. Each element in the model represents a discrete portion of the
physical structure, which is, in turn, represented by many interconnected
elements. Elements are connected to one another by shared nodes. The coordinates
of the nodes and the connectivity of the elements—that is, which nodes belong to
which elements—comprise the model geometry. The collection of all the elements
and nodes in a model is called the mesh. Generally, the mesh will be only an
approximation of the actual geometry of the structure.

The element type, shape, and location, as well as the overall number of elements
used in the mesh, affect the results obtained from a simulation. The greater the
mesh density (i.e., the greater the number of elements in the mesh), the more
accurate the results. As the mesh density increases, the analysis results
converge to a unique solution, and the computer time required for the analysis
increases. The solution obtained from the numerical model is generally an
approximation to the solution of the physical problem being simulated. The
extent of the approximations made in the model's geometry, material behavior,
boundary conditions, and loading determines how well the numerical simulation
matches the physical problem.

--------------
Element blocks
--------------

Element blocks are collections of elements having the same element type and material.  Some elements have geometry not defined completely by the coordinates of their nodes. For example, the area of a link element or moment of inertia of a beam element. Additional geometric data are defined as physical properties of the element block and are necessary to define the model geometry completely.

-------------
Material data
-------------

Material properties for all elements must be specified.

-----------------------------
Loads and boundary conditions
-----------------------------

Loads distort the physical structure and, thus, create stress in it. The most common forms of loading include:

* point loads;
* pressure loads on surfaces;
* body forces, such as the force of gravity; and
* thermal loads.

Boundary conditions are used to constrain portions of the model to remain fixed
(zero displacements) or to move by a prescribed amount (nonzero displacements).
In a static analysis enough boundary conditions must be used to prevent the
model from moving as a rigid body in any direction; otherwise, unrestrained
rigid body motion causes the stiffness matrix to be singular. A solver problem
will occur during the solution stage and may cause the simulation to stop
prematurely.
