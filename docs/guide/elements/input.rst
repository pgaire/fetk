================
Input file usage
================

Input file keyword: ``element``

This option is used to define a *list* of elements directly by specifying their
nodal connectivity.

-----------------
Defining elements
-----------------

Each item in the list of elements contains:

1. The element number.
2. First node number defining the element.
3. Second node number defining the element.
4. Etc., up to the last node number defining the element

Repeat this entry for every element in the model.

-------
Example
-------

.. code-block:: yaml

    element:
    - 1, 1, 2
    - 2, 2, 3
    - 3, 3, 4
