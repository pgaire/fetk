=======================
Internal representation
=======================

Internally, elements are grouped in to :ref:`blocks<eb_index>` and represented by an element object defined by its element type, element fabrication properties, and :ref:`material model<mat_index>`.  The element can be created using the ``fetk.element.factory`` method that takes as input the element type name, element fabrication properties, and the element's material object.

.. code-block:: python

    >>> import fetk.material
    >>> import fetk.element
    >>> material = fetk.material.factory("elastic", 1, {"E": 30.e9, "nu": .3})
    >>> element = fetk.element.factory("l2d2", {"area": 1}, material)
