import numpy as np

import fetk.dload
from .LND2 import L2D2
from .base import element, PLANE, OFF, ON
from fetk.util.geometry import edge_normal
import fetk.util.tensor as tensor


class C2D8E(element):
    """8-node isoparametric plane strain element

    Notes
    -----
    Node and element face numbering

               [2]
            3---6---2
            |       |
       [3]  7       5 [1]
            |       |
            0---4---1
               [0]

    """

    signature = (PLANE, 2, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, 1, 8)
    properties = {"thickness": {"aliases": ["t", "h"], "default": 1.0}}

    def init(self):
        self.edge = L2D2(A=1)

    def area(self, xc):
        """Returns the area of the quadrilateral

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc, xg):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg):
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N = np.zeros(8)
        # corner nodes
        N[0] = -0.25 * (1.0 - s) * (1.0 - t) * (1.0 + s + t)
        N[1] = 0.25 * (1.0 + s) * (1.0 - t) * (s - t - 1.0)
        N[2] = 0.25 * (1.0 + s) * (1.0 + t) * (s + t - 1.0)
        N[3] = 0.25 * (1.0 - s) * (1.0 + t) * (t - s - 1.0)
        # midside nodes
        N[4] = 0.5 * (1.0 - s * s) * (1.0 - t)
        N[5] = 0.5 * (1.0 + s) * (1.0 - t * t)
        N[6] = 0.5 * (1.0 - s * s) * (1.0 + t)
        N[7] = 0.5 * (1.0 - s) * (1.0 - t * t)
        return N

    def shapeder(self, xg):
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dN = np.zeros((2, 8))
        dN[0, 0] = 0.25 * (1.0 - t) * (2.0 * s + t)
        dN[0, 1] = 0.25 * (1.0 - t) * (2.0 * s - t)
        dN[0, 2] = 0.25 * (1.0 + t) * (2.0 * s + t)
        dN[0, 3] = 0.25 * (1.0 + t) * (2.0 * s - t)
        dN[0, 4] = -s * (1.0 - t)
        dN[0, 5] = 0.5 * (1.0 - t * t)
        dN[0, 6] = -s * (1.0 + t)
        dN[0, 7] = -0.5 * (1.0 - t * t)

        dN[1, 0] = 0.25 * (1.0 - s) * (s + 2.0 * t)
        dN[1, 1] = 0.25 * (1.0 + s) * (2.0 * t - s)
        dN[1, 2] = 0.25 * (1.0 + s) * (2.0 * t + s)
        dN[1, 3] = 0.25 * (1.0 - s) * (2.0 * t - s)
        dN[1, 4] = -0.5 * (1.0 - s * s)
        dN[1, 5] = -(1.0 + s) * t
        dN[1, 6] = 0.5 * (1.0 - s * s)
        dN[1, 7] = -(1.0 - s) * t
        return dN

    def shapegrad(self, xc, xg):
        """Compute the derivatives of shape functions wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        dN : ndarray
            Derivatives of the shape function with respect to the physical
            coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        return dNdx

    @property
    def gauss_points(self):
        """Gauss (integration) points

        Returns
        -------
        gp : ndarray
            s, t = gp[i] are s and t coordinates of the ith Gauss point, assuming
            counter clockwise orientation starting from the lower left corner

        """
        p = -np.sqrt(3.0 / 5.0)
        return np.array(
            [[p, p], [0, p], [-p, p], [p, 0], [0, 0], [-p, 0], [p, -p], [0, -p], [-p, -p]]
        )

    @property
    def gauss_weights(self):
        """Gauss (integration) weights

        Returns
        -------
        gw : ndarray
            w = gw[i] is the ith Gauss weight, assuming counter clockwise
            orientation starting from the lower left corner

        """
        a, b, c = [.5555555556, .8888888889, .5555555556]
        return np.array([a * a, a * b, a * c, b * a, b * b, b * c, c * a, c * b, c * c])

    @property
    def edges(self):
        return np.array([[0, 1, 4], [1, 2, 5], [2, 3, 6], [3, 0, 7]])

    def bmatrix(self, xc, xg):
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNdx = self.shapegrad(xc, xg)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((4, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = B[3, 1::num_dof_per_node] = dNdx[0, :]
        B[1, 1::num_dof_per_node] = B[3, 0::num_dof_per_node] = dNdx[1, :]
        return B

    def stiffness(self, xc, material, *args):
        """Assemble the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        ke = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            Ce = material.stiffness(xc, ndir=3, nshr=1)
            ke += np.dot(np.dot(Be.T, Ce), Be) * Je * wg
        return ke

    def pmatrix(self, xg):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xg)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        return P

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f[:num_dof_per_node])
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, edge_no, f[:num_dof_per_node])
            elif tag == fetk.dload.pressure:
                edge = self.edges[edge_no]
                n = edge_normal(xc[edge])
                fe += self.surface_force(xc, edge_no, f[0] * n)
        return fe

    def body_force(self, xc, dload):
        """Calculate the nodal force contributions from a surface load `sload`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        dload : ndarray
            dload[0] is the load on the body in the x direction
            dload[1] is the load on the body in the y direction

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            # q = np.dot(Pe, dload[:num_dof])
            fe += Je * wg * np.dot(Pe.T, dload[:num_dof_per_node])
        return fe

    def surface_force(self, xc, edge_no, sload):
        """Calculate the nodal force contributions from a surface load `sload`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        edge_no : ndarray
            The edge number (using internal edge numbering)
        sload : ndarray
            The surface load

        """
        edge = self.edges[edge_no]
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            wg = self.edge.gauss_weights[p]
            xg = self.edge.gauss_points[p]
            N = self.edge.shape(xg)
            dNds = self.edge.shapeder(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            for (i, ni) in enumerate(edge):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * sload[j]
        return fe
