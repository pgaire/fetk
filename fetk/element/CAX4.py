import numpy as np

import fetk.dload
from .LND2 import L1D2
from .base import element, PLANE, OFF, ON
from fetk.util.geometry import edge_normal


class CAX4(element):
    """4-node isoparametric axisymmetric element

    Notes
    -----
    - Node and element face numbering

                   [2]
                3-------2
                |       |
           [3]  |       | [1]
                |       |
                0-------1
                   [0]

    - Element is integrated by a 2x2 Gauss rule

    """

    signature = (PLANE, 2, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, 10, 4)

    def init(self):
        self.edge = L1D2(A=1)

    def area(self, xc):
        """Returns the area of the quadrilateral

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return 1.0 * self.area(xc)

    def jacobian(self, xc, xg):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg):
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N = np.array(
            [
                (1.0 - s) * (1.0 - t),
                (1.0 + s) * (1.0 - t),
                (1.0 + s) * (1.0 + t),
                (1.0 - s) * (1.0 + t),
            ]
        )
        return N / 4.0

    def shapeder(self, xg):
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dN = np.array(
            [
                [-1.0 + t, 1.0 - t, 1.0 + t, -1.0 - t],
                [-1.0 + s, -1.0 - s, 1.0 + s, 1.0 - s],
            ]
        )
        return dN / 4.0

    def shapegrad(self, xc, xg):
        """Compute the derivatives of shape functions wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        dN : ndarray
            Derivatives of the shape function with respect to the physical
            coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shapeder(xg)
        drds = np.dot(dNds, xc)
        dsdr = np.linalg.inv(drds)
        dNdr = np.dot(dsdr, dNds)
        return dNdr

    @property
    def gauss_points(self):
        """Gauss (integration) points

        Returns
        -------
        gp : ndarray
            s, t = gp[i] are s and t coordinates of the ith Gauss point, assuming
            counter clockwise orientation starting from the lower left corner

        """
        p = 1 / np.sqrt(3.0)
        return np.array([[-p, -p], [p, -p], [p, p], [-p, p]])

    @property
    def gauss_weights(self):
        """Gauss (integration) weights

        Returns
        -------
        gw : ndarray
            w = gw[i] is the ith Gauss weight, assuming counter clockwise
            orientation starting from the lower left corner

        """
        return np.ones(4)

    @property
    def edges(self):
        return [[0, 1], [1, 2], [2, 3], [3, 0]]

    def bmatrix(self, xc, xg):
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        N = self.shape(xg)
        xr = xc[:, 0]
        rp = np.dot(N, xr)
        dNdr = self.shapegrad(xc, xg)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((4, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = B[3, 1::num_dof_per_node] = dNdr[0, :]
        B[1, 1::num_dof_per_node] = B[3, 0::num_dof_per_node] = dNdr[1, :]
        B[2, 0::num_dof_per_node] = N / rp
        return B

    def stiffness(self, xc, material, *args):
        """Assemble the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        ke = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))
        p1 = p2 = 2
        for p in range(p1 * p2):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Np = self.shape(xg)
            Be = self.bmatrix(xc, xg)
            Jp = self.jacobian(xc, xg)
            Cp = material.stiffness(xc, ndir=3, nshr=1)
            rp = np.dot(Np, xc[:, 0])
            ke += np.dot(np.dot(Be.T, Cp), Be) * Jp * wg * rp
        return ke

    def pmatrix(self, xg):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xg)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        return P

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f)
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, edge_no, f)
            elif tag == fetk.dload.pressure:
                edge = self.edges[edge_no]
                n = edge_normal(xc[edge])
                fe += self.surface_force(xc, edge_no, f[0] * n)
        return fe

    def body_force(self, xc, dload):
        """Calculate the nodal force contributions from a surface load `sload`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the r coordinates, in the order shown above
            xc[:, 1] (len=4) are the z coordinates
        dload : ndarray
            dload[0::2] are the nodal body forces in the r direction
            dload[1::2] are the nodal body forces in the z direction

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        qe = np.zeros(num_dof)
        if len(dload) == num_dof_per_node:
            for i in range(num_dof_per_node):
                qe[i::num_dof_per_node] = dload[i]
        else:
            qe[:] = dload[:num_dof]
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Np = self.shape(xg)
            rp = np.dot(Np, xc[:, 0])
            Jp = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            qp = np.dot(Pe, qe)
            fe += rp * Jp * wg * np.dot(Pe.T, qp)
        return fe

    def surface_force(self, xc, edge_no, dload):
        """Calculate the nodal force contributions from a surface load `dload`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        edge : ndarray
            Internal node IDs of the surface
        dload : ndarray
            The surface load

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        edge = self.edges[edge_no]
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            wg = self.edge.gauss_weights[p]
            xg = self.edge.gauss_points[p]
            N = self.edge.shape(xg)
            dNds = self.edge.shapeder(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            for (i, ni) in enumerate(edge):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * dload[j]
        return fe
