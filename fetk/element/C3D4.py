import numpy as np

import fetk.dload
from .LND2 import L1D2
from .C2D4S import C2D4S
from .base import element, SOLID, OFF, ON
from fetk.util.geometry import face_normal


class C3D4(element):
    """
            3
            .
           . . .
          .   .   .
         .     .    .
        .       .     2
       .         .   .
      .           . .
     0. . . . . . .1

    """
    signature = (SOLID, 3, ON, ON, ON, OFF, OFF, OFF, OFF, OFF, 1, 4)

    def init(self):
        self.edge = L1D2(A=1)
        self.face = C2D4S(thickness=1)

    def volume(self, xc):
        """Volume of the tetradhral element

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        v : float
            The volume

        """
        M = np.ones((4, 4))
        for i in range(3):
            M[i + 1, :] = xc[:, i]
        V = np.linalg.det(M) / 6.0
        return V

    @property
    def faces(self):
        return [[0, 1, 2], [0, 1, 3], [1, 2, 3], [2, 0, 3]]

    def jacobian(self, xc):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        return 6 * self.volume(xc)

    def shape(self, xc, p):
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        x, y = self.isop_map(xc, p)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        N1 = x2 * y3 - x3 * y2 + (y2 - y3) * x + (x3 - x2) * y
        N2 = x3 * y1 - x1 * y3 + (y3 - y1) * x + (x1 - x3) * y
        N3 = x1 * y2 - x2 * y1 + (y1 - y2) * x + (x2 - x1) * y
        Ae = self.area(xc)
        return np.array([N1, N2, N3]) / 2 / Ae

    def shapegrad(self, xc):
        """Derivatives of shape functions, wrt to the physical coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        Ae = self.area(xc)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        dN = np.array([[y2 - y3, y3 - y1, y1 - y2], [x3 - x2, x1 - x3, x2 - x1]])
        return dN / 2.0 / Ae

    @property
    def gauss_points(self):
        return np.array([[1.0, 1.0], [4.0, 1.0], [1.0, 4.0]]) / 6.0

    @property
    def gauss_weights(self):
        return np.ones(3) / 3.0

    def isop_map(self, xc, p):
        """Map the point `p` in natural coordinates to physical coordinates

        Paratemeters
        ------------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : list of float
            s, t = p

        """
        s, t = p
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        x = x1 * s + x2 * t + x3 * (1 - s - t)
        y = y1 * s + y2 * t + y3 * (1 - s - t)
        return x, y

    def bmatrix(self, xc):
        """Compute the element B matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        """
        dNdx = self.shapegrad(xc)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((3, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = B[2, 1::num_dof_per_node] = dNdx[0, :]
        B[1, 1::num_dof_per_node] = B[2, 0::num_dof_per_node] = dNdx[1, :]
        return B

    def pmatrix(self, xc, p):
        """Compute the element P matrix

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : list of float
            s, t = p

        Returns
        -------
        P : ndarray
            The P matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xc, p)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        return P

    def stiffness(self, xc, material, *args):
        """Compute the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        Ae = self.area(xc)
        Be = self.bmatrix(xc)
        he = self.thickness
        ke = np.zeros((6, 6))
        for p in range(len(self.gauss_points)):
            wg = self.gauss_weights[p]
            C = material.stiffness(xc, ndir=2, nshr=1)
            ke += Ae * he * wg * np.dot(np.dot(Be.T, C), Be)
        return ke

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, face_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f[:num_dof_per_node])
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, face_no, f[:2])
            elif tag == fetk.dload.pressure:
                face = self.faces[face_no]
                n = face_normal(xc[face])
                fe += self.surface_force(xc, face_no, f[0] * n)
        return fe

    def body_force(self, xc, f):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        f : array_like
            The array of distributed loads

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        he = self.thickness
        Ae = self.area(xc)
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Pe = self.pmatrix(xc, xg)
            fe += Ae * he / 3.0 * wg * np.dot(Pe.T, f[:num_dof_per_node])
        return fe

    def surface_force(self, xc, face_no, q):
        """Calculate the nodal force contributions from a surface load `q`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        face_no : ndarray
            The face number (using internal face numbering)
        q : ndarray
            The surface load

        """
        face = self.faces[face_no]
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        xf = xc[face]
        for p in range(len(self.face.gauss_points)):
            wg = self.face.gauss_weights[p]
            xg = self.face.gauss_points[p]
            N = self.face.shape(xg)
            dNds = self.face.shapeder(xg)
            dxds = np.dot(dNds, xf)
            a = (dxds[0, 1] * dxds[1, 2]) - (dxds[1, 1] * dxds[0, 2])
            b = (dxds[0, 0] * dxds[1, 2]) - (dxds[1, 0] * dxds[0, 2])
            c = (dxds[0, 0] * dxds[1, 1]) - (dxds[1, 0] * dxds[0, 1])
            Jd = np.sqrt(a ** 2 + b ** 2 + c ** 2)
            for (i, ni) in enumerate(face):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * q[j]
        return fe
