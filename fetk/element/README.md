# Elements

## Defining new elements

To define a new element:

1. Create a new element, inheriting from `element`, the element should define the
   node freedom table `nft`.  `nft` is a tuple of length 11 describing the
   element and its degree of freedom contributions to the global problem.  The
   values in nft corrspond to:

   ```python
     nft[0]: the element family.
     nft[1]: the element dimension
     nft[2]: dof 1: displacement x: 1 if active, 0 otherwise
     nft[3]: dof 2: displacement y: 1 if active, 0 otherwise
     nft[4]: dof 3: displacement z: 1 if active, 0 otherwise
     nft[5]: dof 4: rotation x: 1 if active, 0 otherwise
     nft[6]: dof 5: rotation y: 1 if active, 0 otherwise
     nft[7]: dof 6: rotation z: 1 if active, 0 otherwise
     nft[8]: dof 7: temperature: 1 if active, 0 otherwise
     nft[9]: Special qualifier to differentiate elements with equal number of
             nodes and degrees of freedom
     nft[10]: Number of nodes
   ```

   For example, a 1 dimensional 2 node link element could be defined as:

   ```python
     class L1D2(element):
         nft = (LINE, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2)
   ```

2. Define the element `set_properties` function that sets fabrication properties
   required by the element

   ```python
   class L1D2(element):
       nft = (LINE, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2)
       def set_properties(self, properties):
           self.area = properties.get("area", 1.0)
           ...
   ```

3. Define the element stiffness and element force functions

   The element stiffness should take as input the element's nodal.  The force
   should take the element's nodal coordinates, distributed load tags, and
   distributed load values.

   ```python
     class L1D2(element):
         nft = (LINE, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2)

         def set_properties(self, properties):
             ...
         def stiffness(self, coords, material, *args):
             ke = ...
             return ke

         def force(self, coords, dltags, dlvals):
             fe = ...
             return fe
   ```

   At this point of the call stack, the inputs to the element stiffness and
   element force procedures are the values at that particular element.

4. Update the factory function `__init__.factory`.  This a function that takes the
   element type as a string and returns an element object.  This is useful, for
   example, for allowing user input to be the name of the element and not the
   type.
