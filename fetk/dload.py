from types import SimpleNamespace


body_force, pressure, surface_force = 10, 11, 12
body_flux, surface_flux, surface_film = 20, 21, 22
types = (body_force, pressure, surface_force, body_flux, surface_flux, surface_film)


class dload_manager:
    """Container for distributed loads

    Parameters
    ----------
    dltags : ndarray
        Distributed load tags
        dltags[d, 0] is the external element ID of the dth distributed load
        dltags[d, 1] is the distributed load type
        dltags[d, 2] is the face number to apply surface loads (if applicable)
    dlvals : ndarray
        Distributed load values
        dlvals[i] is the magnitude of the distributed load corresponding to
          dltags[d].

    """

    def __init__(self, tags=None, values=None):
        self.map = {}
        self.tags = []
        self.values = []
        if tags is not None:
            for (i, row) in enumerate(tags):
                self.set(row[0], row[1:], values[i])

    def set(self, type, elem_no, side_no, values, override=False):
        if type not in types:
            raise ValueError(f"Unknown dload type {type}")
        load_no = self.map.get(elem_no)
        if load_no is None:
            load_no = len(self.map)
            self.tags.append([[type, side_no]])
            self.values.append([values])
            self.map[elem_no] = load_no
        elif override:
            self.tags[load_no] = [[type, side_no]]
            self.values[load_no] = [values]
        else:
            self.tags[load_no].append([type, side_no])
            self.values[load_no].append(values)

    def get(self, elem_no):
        load_no = self.map.get(elem_no)
        if load_no is None:
            return None
        return dload(tags=self.tags[load_no], values=self.values[load_no])


class dload:
    def __init__(self, *, tags, values):
        self.tags = tags
        self.values = values
        self.ndload = len(values)
        assert self.ndload == len(tags)
    def items(self):
        for i in range(self.ndload):
            tag, side_no = self.tags[i]
            yield tag, side_no, self.values[i]
