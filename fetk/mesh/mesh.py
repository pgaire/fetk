import logging
import numpy as np

from elemlib1 import ElementFamily
from fetk.util.lang import mutually_exclusive_args

from .generators import rectilinear2d
from .search import find_nodes_in_region, find_sides_in_region


ILO, IHI, JLO, JHI, KLO, KHI = "ilo", "ihi", "jlo", "jhi", "klo", "khi"
ALL = "all"
BOUNDARY = "boundary"


def is_listlike(a):
    return (
        not hasattr(a, "strip") and hasattr(a, "__getitem__") or hasattr(a, "__iter__")
    )


def is_stringlike(s):
    return hasattr(s, "strip")


class ElementBlock:
    def __init__(self, name, id, elems, eletyp):
        self.name = name
        self.id = id
        self.elems = elems
        self.eletyp = eletyp


class mesh:
    """Class for creating a finite element mesh.

    Parameters
    ----------
    nodes : list
        Finite element nodes
        nodes[n][0] is the node label of the nth node and
        nodes[n][1:] are the nodal coordinates of the nth node
    elements : list
        Finte element elements
        eletab[e][0] is the element label of the eth element and
        eletab[e][1:] are the nodes forming the eth element, the nodal
            labels must correpond to the node labels in `nodes`

    Notes
    -----
    The mesh object is intimately connected with the Exodus finite element data
    model.  Internally, elements are numbered by the order in which they appear
    in element blocks.

    Examples
    --------

    In the following examples, a simple mesh is created of a unit square
    centered at the origin with node and element numbers as follows:

    .. image:: unitsquare1.png
       :scale: 35
       :align: center

    Method 1: specifying ``nodtab`` and ``eletab``:

    >>> nodes = [[10, -.5, -.5], [20, .5, -.5], [30, .5, .5], [40, -.5, .5]]
    >>> elements = [[100, 10, 20, 30, 40]]
    >>> m = mesh(nodes, elements)

    """

    def __init__(self, nodes, elements):

        self.node_map = {}
        self.elem_map = {}
        self.node_sets = {}
        self.elem_sets = {}
        self.side_sets = {}
        self.elem_blocks = []

        # Basic element info.  The element map and connectivity will be made
        # once element blocks are added
        self.num_elem = len(elements)
        self.elements = elements

        # Basic node info
        max_dim = -1
        num_node = len(nodes)
        self.coord = np.zeros((num_node, 3))
        self.inode_map = np.zeros(num_node, dtype=int)
        for (i, node) in enumerate(nodes):
            xn, *x = node
            self.node_map[xn] = i
            self.inode_map[i] = xn
            dim = len(x)
            max_dim = max(max_dim, dim)
            if max_dim > 3:
                raise ValueError(f"Node {node[0]}'s dimension > 3")
            self.coord[i, :dim] = x

    @mutually_exclusive_args("region", "nodes", required=True)
    def node_set(self, name, *, region=None, nodes=None):
        """Define a node set for the mesh

        Parameters
        ----------
        name : str
            The node set name
        region : str
            Region identifier [ijk](lo|hi) where to find nodes
        nodes : list
            External node IDs

        """
        if not isinstance(name, str):
            raise ValueError("Name must be a string")
        elif name in self.node_sets:
            raise ValueError(f"Duplicate node set name {name!r}")
        if region is not None:
            if region == ALL:
                nodes = sorted(list(self.node_map.keys()))
            elif region in (ILO, IHI, JLO, JHI, KLO, KHI):
                nodes = find_nodes_in_region(
                    self.coord, region, map=lambda i: self.inode_map[i]
                )
            else:
                raise ValueError(f"Invalid region {region!r}")
        else:
            errors = []
            for node in nodes:
                if node not in self.node_map:
                    errors.append(node)
            if errors:
                nodes = ", ".join(str(n) for n in nodes)
                raise ValueError(f"The following nodes were not found: {nodes}")
        self.node_sets[name] = np.array(sorted(nodes), dtype=int)
        return

    @mutually_exclusive_args("region", "sides", required=True)
    def side_set(self, name, region=None, sides=None):
        if not isinstance(name, str):
            raise ValueError("Name must be a string")
        elif name in self.side_sets:
            raise ValueError(f"Duplicate side set name {name!r}")
        if region is not None:
            sides = find_sides_in_region(self.coord, self.blocks, region)
        else:
            for (i, side) in enumerate(list(sides)):
                elem_no, side_no = side
                if is_listlike(elem_no):
                    sides[i] = [elem_no[0], side_no]
                    sides.extend([[e, side_no] for e in elem_no[1:]])
        self.side_set[name] = np.array(sorted(sides))

    @mutually_exclusive_args("region", "elements", required=True)
    def element_set(self, name, region=None, elements=None):
        if not isinstance(name, str):
            raise ValueError("Element set name must be a string")
        elif name in self.element_sets:
            raise ValueError(f"Duplicate element set name {name!r}")
        if region == ALL:
            ielems = np.arange(self.num_elem,)
        else:
            if not is_listlike(region):
                region = [region]
            ielems = [self.elemap[el] for el in region]
        self.elemsets[name] = ielems

    def element_block(self, name, elements, element, material, **properties):
        if name in [eb.name for eb in self.elem_blocks]:
            raise ValueError("{0!r} already an element block".format(name))
        if elements == ALL:
            ielems = range(self.num_elem)
        else:
            if not is_listlike(elements):
                elements = [elements]
        block = fetk.mesh.element_block(
            name, len(self.elem_blocks) + 1, elements, element
        )

        if elefab:
            # element fabrication properties given
            for (key, val) in elefab.items():
                if not is_listlike(val) or len(val) != len(ielems):
                    elefab[key] = [val] * len(ielems)
                else:
                    elefab[key] = val
        for (i, ielem) in enumerate(ielems):
            elenod = [n for n in self.elecon[ielem] if n >= 0]
            if len(elenod) != eletyp.numnod:
                raise TypeError("Incorrect number of nodes for element type")
            elecoord = self.coord[elenod]
            xel = self.ielemap[ielem]
            kwds = {}
            for (key, val) in elefab.items():
                kwds[key] = val[i]
            self.elements[ielem] = eletyp(xel, elenod, elecoord, elemat, **kwds)
        self.elem_blocks.append(blk)
        return blk

    @classmethod
    def reclinear2d(cls, nx=1, ny=1, lx=1.0, ly=1.0):
        """Generate a 2D rectilinear mesh

        Parameters
        ----------
        nx, ny : int
            Number of elements in the x and y directions, respectively
        lx, ly : float

        Returns
        -------
        mesh

        """
        nodes, elements = rectilinear2d(nx=nx, ny=ny, lx=lx, ly=ly)
        return cls(nodes, elements)

    def write(self, filename, format="exo"):
        import exodusii

        f = exodusii.exo_file(filename, mode="w")
        if not self.complete:
            raise ValueError("Elementsm must be assigned to element blocks")
        f.genesis(
            self.node_map,
            self.coord,
            self.elem_blocks,
            nodesets=self.node_sets,
            elemsets=self.elem_sets,
            sidesets=self.side_sets,
        )
        f.close()
