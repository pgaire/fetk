import numpy as np
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.cm import Spectral
import matplotlib.pyplot as plt


def plot2d(
    xy,
    connect,
    u=None,
    color=None,
    ax=None,
    show=0,
    weight=None,
    colorby=None,
    linestyle="-",
    label=None,
    xlim=None,
    ylim=None,
    **kwds
):
    xy = np.asarray(xy)
    assert xy.ndim == 2
    if u is not None:
        xy += u.reshape(xy.shape)

    patches = []
    for points in xy[connect[:]]:
        quad = Polygon(points, True)
        patches.append(quad)

    if ax is None:
        fig, ax = plt.subplots()

    # colors = 100 * random.rand(len(patches))
    p = PatchCollection(patches, linewidth=weight, **kwds)
    if colorby is not None:
        colorby = np.asarray(colorby).flatten()
        if len(colorby) == len(xy):
            # average value in element
            colorby = np.array([np.average(colorby[points]) for points in connect])
        p.set_cmap(Spectral)  # coolwarm)
        p.set_array(colorby)
        p.set_clim(vmin=colorby.min(), vmax=colorby.max())
        fig.colorbar(p)
    else:
        p.set_edgecolor(color)
        p.set_facecolor("None")
        p.set_linewidth(weight)
        p.set_linestyle(linestyle)

    if label:
        ax.plot([], [], color=color, linestyle=linestyle, label=label)

    ax.add_collection(p)

    if not ylim:
        ymin, ymax = np.amin(xy[:, 1]), np.amax(xy[:, 1])
        dy = max(abs(ymin * 0.05), abs(ymax * 0.05))
        ax.set_ylim([ymin - dy, ymax + dy])
    else:
        ax.set_ylim(ylim)

    if not xlim:
        xmin, xmax = np.amin(xy[:, 0]), np.amax(xy[:, 0])
        dx = max(abs(xmin * 0.05), abs(xmax * 0.05))
        ax.set_xlim([xmin - dx, xmax + dx])
    else:
        ax.set_xlim(xlim)
    ax.set_aspect("equal")

    if show:
        plt.legend()
        plt.show()

    return ax
