import numpy as np
from types import SimpleNamespace

from exodusii.file import ExodusIIFile as exo_file


def load(filename):
    file = exo_file(filename)
    coords = file.get_coords()
    nodes = []
    for (iid, id) in enumerate(file.get_node_id_map()):
        node = [id]
        node.extend(coords[iid])
        nodes.append(node)

    elements = []
    for block_id in file.get_element_block_ids():
        connect = file.get_element_conn(block_id)
        for (el, el_nodes) in enumerate(connect, start=len(elements) + 1):
            element = [el]
            element.extend(el_nodes)
            elements.append(element)

    nodesets = {}
    for nsid in file.get_node_set_ids():
        ns = file.get_node_set(nsid)
        nodesets[ns.name] = ns.nodes

    sidesets = {}
    for ssid in file.get_side_set_ids():
        ss = file.get_side_set(ssid)
        sidesets[ss.name] = np.column_stack((ss.elems, ss.sides))

    mesh = SimpleNamespace(
        nodes=nodes, elements=elements, nodesets=nodesets, sidesets=sidesets
    )

    return mesh


def snapshot(jobid, coords, elem_blocks, node_map, **kwds):

    # create new file
    filename = jobid + ".exo"
    exo = exo_file(filename, mode="w")
    num_elem = sum(eb.connect.shape[0] for eb in elem_blocks)
    num_node = coords.shape[0]
    if coords.ndim == 1:
        coords = coords.reshape(-1, 1)
    num_node, num_dim = coords.shape

    # initialize file with parameters
    exo.put_init("FETK Snapshot", num_dim, num_node, num_elem, len(elem_blocks), 0, 0)
    exo.put_coords(coords)
    exo.put_global_variable_params(1)
    exo.put_global_variable_names(["DT"])

    ex_node_map = np.array(sorted(node_map.keys(), key=lambda k: node_map[k]))
    exo.put_node_id_map(ex_node_map)

    node_variables = None
    node_variable_names = []
    disp = kwds.pop("disp", None)
    if disp is not None:
        node_variables = np.array(disp)
        if node_variables.ndim == 1:
            node_variables = node_variables.reshape((-1, 1))
        node_variable_names = ["displ%s" % d for d in "xyz"[:num_dim]]

    for (key, value) in kwds.items():
        if value.ndim == 1:
            value = value.reshape((-1, 1))
        if len(value) != len(coords):
            raise ValueError("Extra variables must have same length as coordinates")
        if value.shape[1] == 1:
            node_variable_names.append(key)
        else:
            for i in range(value.shape[1]):
                node_variable_names.append(key + "_{0}".format(i))
        if node_variables is None:
            node_variables = np.array(value)
        else:
            node_variables = np.append(node_variables, value, 1)

    for (id, block) in enumerate(elem_blocks):
        connect = block.connect + 1
        ne, nn = connect.shape
        elem_type = get_exo_element_type(num_dim, nn)
        exo.put_element_block(id + 1, elem_type, ne, nn)
        exo.put_element_conn(id + 1, connect)

    num_node_vars = len(node_variable_names)
    if num_node_vars:
        exo.put_node_variable_params(num_node_vars)
        exo.put_node_variable_names(node_variable_names)

        exo.put_time(1, 0.0)
        exo.put_global_variable_values(1, [0.0])
        for name in node_variable_names:
            exo.put_node_variable_values(1, name, np.zeros(len(coords)))

        exo.put_time(2, 1.0)
        exo.put_global_variable_values(2, [1.0])
        for (k, name) in enumerate(node_variable_names):
            exo.put_node_variable_values(2, name, node_variables[:, k])


def put_nodal_solution(jobid, xc, connect, u):
    block = SimpleNamespace(name="Block-1", connect=connect)
    node_map = dict([i + 1, i] for i in range(len(xc)))
    snapshot(jobid, xc, [block], node_map, u=u)


def get_exo_element_type(numdim, numnod):
    if numnod == 2:
        elem_type = "TRUSS"
    elif numdim == 1:
        elem_type = "TRUSS"
    elif numdim == 2:
        if numnod == 3:
            elem_type = "TRI"
        elif numnod in (4, 8):
            elem_type = "QUAD4"
    elif numdim == 3:
        if numnod in (4, 6):
            elem_type = "TET"
        elif numnod in (8, 20):
            elem_type = "HEX"
    else:
        raise ValueError("Unknown element type")
    return elem_type
