from datetime import datetime
import fcntl
import os
import struct
import sys
import termios
import textwrap

from fetk.io.color import cprint, cwrite, cescape

_timestamp = False
_debug = False
_prefix = "==>"
indent = "  "


def set_debug(flag):
    global _debug
    _debug = flag


def set_timestamp(flag):
    global _timestamp
    _timestamp = flag


def get_prefix():
    return _prefix


def get_timestamp(force=False):
    """Get a string timestamp"""
    if _debug or _timestamp or force:
        return datetime.now().strftime("[%Y-%m-%d-%H:%M:%S.%f] ")
    else:
        return ""


def msg(message, *args, **kwargs):
    newline = kwargs.get("newline", True)
    if newline:
        cprint("@*b{%s} %s%s" % (get_prefix(), get_timestamp(), cescape(message)))
    else:
        cwrite("@*b{%s} %s%s" % (get_prefix(), get_timestamp(), cescape(message)))
    for arg in args:
        print(indent + str(arg))


def info(message, *args, **kwargs):
    format = kwargs.get("format", "*b")
    stream = kwargs.get("stream", sys.stdout)
    wrap = kwargs.get("wrap", False)
    prefix = kwargs.get("prefix") or get_prefix()
    end = kwargs.get("end", "\n")
    break_long_words = kwargs.get("break_long_words", False)
    cprint(
        "@%s{%s} %s%s" % (format, prefix, get_timestamp(), cescape(str(message))),
        stream=stream,
    )
    for arg in args:
        if wrap:
            lines = textwrap.wrap(
                str(arg),
                initial_indent=indent,
                subsequent_indent=indent,
                break_long_words=break_long_words,
            )
            for line in lines:
                stream.write(line + "\n")
        else:
            stream.write(indent + str(arg) + end)
    stream.flush()


def debug(message, *args, **kwargs):
    if _debug:
        kwargs.setdefault("format", "g")
        kwargs.setdefault("stream", sys.stdout)
        info(message, *args, **kwargs)


def error(message, *args, **kwargs):
    kwargs.setdefault("format", "*r")
    kwargs.setdefault("stream", sys.stderr)
    info("Error: " + str(message), *args, **kwargs)


def warn(message, *args, **kwargs):
    kwargs.setdefault("format", "*Y")
    kwargs.setdefault("stream", sys.stderr)
    info("Warning: " + str(message), *args, **kwargs)


def die(message, *args, **kwargs):
    kwargs.setdefault("countback", 4)
    error(message, *args, **kwargs)
    sys.exit(1)


def terminal_size():
    """Gets the dimensions of the console: (rows, cols)."""

    def ioctl_gwinsz(fd):
        try:
            rc = struct.unpack("hh", fcntl.ioctl(fd, termios.TIOCGWINSZ, "1234"))
        except BaseException:
            return
        return rc

    rc = ioctl_gwinsz(0) or ioctl_gwinsz(1) or ioctl_gwinsz(2)
    if not rc:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            rc = ioctl_gwinsz(fd)
            os.close(fd)
        except BaseException:
            pass

    if not rc:
        rc = (os.environ.get("LINES", 25), os.environ.get("COLUMNS", 80))

    return int(rc[0]) or 25, int(rc[1]) or 80
