import numpy as np
from .base import base


class elastic(base):
    params = {
        "E": {"aliases": ("young's modulus", "E"), "default": None},
        "nu": {"aliases": ("poisson's ratio", "nu"), "default": 0.0},
    }

    def stiffness(self, xc, ndir=2, nshr=1):
        """Linear elastic stiffness

        Parameters
        ----------
        xc : ndarray
            The coordinates of this material point
        ndir : int
            The number of direct stress components
        nshr : int
            The number of shear stress components

        Returns
        -------
        C : ndarray
            The elastic stiffness

        """
        if ndir == 1 and nshr == 0:
            return self.E
        elif ndir == 2 and nshr == 1:
            # Plane stress stiffness
            E, nu = self.E, self.nu
            G = E / 2.0 / (1.0 + nu)
            Es = E / (1.0 - nu ** 2)
            C = np.array([[Es, nu * Es, 0.0], [nu * Es, Es, 0.0], [0.0, 0.0, G]])
        elif ndir == 3 and nshr == 1:
            # Plane strain stiffness
            E, nu = self.E, self.nu
            fac = E / (1 + nu) / (1 - 2 * nu)
            m = np.array(
                [
                    [1 - nu, nu, nu, 0],
                    [nu, 1 - nu, nu, 0],
                    [nu, nu, 1 - nu, 0],
                    [0, 0, 0, 0.5 - nu],
                ]
            )
            return fac * m
        elif ndir == 3 and nshr == 3:
            E, nu = self.E, self.nu
            G = E / 2.0 / (1.0 + nu)
            K = E / 3.0 / (1.0 - 2.0 * nu)
            H = K + 4.0 * G / 3.0
            M = K - 2.0 * G / 3.0
            C = np.array(
                [
                    [H, M, M, 0, 0, 0],
                    [M, H, M, 0, 0, 0],
                    [M, M, H, 0, 0, 0],
                    [0, 0, 0, G, 0, 0],
                    [0, 0, 0, 0, G, 0],
                    [0, 0, 0, 0, 0, G],
                ]
            )
        else:
            raise ValueError(f"Elastic model does not support ndir={ndir}, nshr={nshr}")
        return C
