import re
import numpy as np
import fetk.io.logging as logging
from types import SimpleNamespace

import fetk.node
import fetk.dload
import fetk.element
import fetk.boundary
import fetk.assemble
import fetk.material
from fetk.io import exodusii, vtk
from fetk.util.lang import string_types, scalar_types, is_sequence, fill_missing


class fe_model:
    """The finite element model

    Parameters
    ----------
    nodes : list
        nodes[i, 0] is the external node ID of the ith node
        nodes[i, 1:] are the nodal coordinates of the ith node
    elements : list
        elements[e, 0] is the external element ID of the eth element
        elements[e, 1:] are the external node IDs of the element's nodes

    """

    def __init__(self, name, nodes, elements):
        self.name = name
        self.coord, self.node_map = fetk.node.factory(nodes)
        self.dimension = self.coord.shape[1]

        self.blocks = []
        self.elem_map = {}
        self.elements = dict([(el[0], el[1:]) for el in elements])

        self.dloads = fetk.dload.dload_manager()
        self.bcs = fetk.boundary.dof_manager(self.num_node)

        self.nodesets = {}
        self.sidesets = {}
        self.elemsets = {}

    def finalize(self):
        active_dofs = None
        for block in self.blocks:
            # FIXME - needs to be updated to handle variable DOF
            if active_dofs is None:
                active_dofs = list(block.element.active_dofs)
            elif not active_dofs == list(block.element.active_dofs):
                raise ValueError(
                    "Variable degree of freedom not yet implemented.  "
                    "All element blocks must have the same active DOFs"
                )
        self.active_dofs = active_dofs

    @property
    def num_node(self):
        return self.coord.shape[0]

    @property
    def num_elem(self):
        return sum([block.num_elem for block in self.blocks])

    def element_block(self, name, *, elements, element, material):
        for block in self.blocks:
            if block.name == name:
                raise ValueError(f"Duplicate element block name {name}")
        if isinstance(elements, str) and elements.lower() == "all":
            elements = sorted(self.elements.keys())
        connect, map = [], {}
        block_no = len(self.blocks)
        for (iel, xel) in enumerate(elements, start=len(self.elem_map)):
            xnodes = self.elements.get(xel)
            if xnodes is None:
                raise ValueError(f"Element {xel} is not defined")
            inodes = []
            for xn in xnodes:
                n = self.node_map.get(xn)
                if n is None:
                    raise ValueError(f"Node {n} of element {xel} is not defined")
                inodes.append(n)
            connect.append(inodes)
            if xel in self.elem_map:
                raise ValueError(f"Element {xel} is defined in multiple element blocks")
            if xel in map:
                raise ValueError(f"Element {xel} is multiply defined")
            map[xel] = (iel, block_no)
        self.elem_map.update(map)
        block = fetk.element.block.block(name, elements, connect, element, material)
        self.blocks.append(block)

    def nodeset(self, name, *, nodes):
        self.nodesets[name] = np.asarray(nodes)

    def element_set(self, name, *, elements):
        self.elemsets[name] = np.asarray(elements)

    def sideset(self, name, *, sides=None, key=None):
        self.sidesets[name] = np.asarray(sides)

    def solve(self):
        """Solve the finite element model"""
        self.finalize()
        K = fetk.assemble.stiffness(self.coord, self.blocks, self.dloads)
        F = fetk.assemble.force(self.coord, self.blocks, self.dloads)
        doftags = self.bcs.tags(self.active_dofs)
        dofvals = self.bcs.values(self.active_dofs)
        Kbc, Fbc = fetk.boundary.apply(K, F, doftags, dofvals)
        u = np.linalg.solve(Kbc, Fbc)
        Q = fetk.boundary.force(doftags, dofvals)
        r = np.dot(K, u) - Q
        u = u.reshape((-1, len(self.active_dofs)))
        r = r.reshape((-1, len(self.active_dofs)))
        self.solution = SimpleNamespace(u=u, r=r, K=K, F=F, Q=Q)
        return

    def dump(self, **kwds):
        format = kwds.pop("format", "exodus")
        if format == "exodus":
            exodusii.snapshot(self.name, self.coord, self.blocks, self.node_map, **kwds)
        elif format == "vtk":
            vtk.snapshot(self.name, self.coord, self.blocks, self.node_map, **kwds)

    def get_dofs(self, label):
        orig_label = label
        if isinstance(label, string_types):
            label = list(label)
        elif isinstance(label, scalar_types):
            label = [int(label)]
        dofs = []
        errors = 0
        dofmap = dict(zip("xyzt", (0, 1, 2, 6)))
        for dof in label:
            if isinstance(dof, int) and dof < fetk.boundary.max_dof:
                dofs.append(dof)
            elif isinstance(dof, string_types) and dof.lower() in dofmap:
                dofs.append(dofmap[dof.lower()])
            else:
                errors += 1
                logging.error(f"Invalid dof {dof} in {orig_label}")
        if errors:
            raise ValueError(f"Invalid dof[s] encountered in {orig_label!r}")
        return tuple(dofs)

    def get_elems(self, label):
        elements = None
        if label is None:
            elements = list(self.elem_map.keys())
        elif isinstance(label, string_types):
            elements = self.elemsets.get(label)
        elif isinstance(label, scalar_types):
            elements = np.array([label], dtype=int)
        elif is_sequence(label):
            elements = np.array(label, dtype=int)
        if elements is None:
            raise ValueError(f"{label} is not an element set, element, or elements")
        return elements

    def get_sides(self, label):
        sides = None
        if isinstance(label, string_types):
            sides = self.sidesets.get(label)
        elif is_sequence(label):
            sides = np.array(label, dtype=int)
            if sides.ndim != 2 and sides.shape[1] != 2:
                sides = None
        if sides is None:
            raise ValueError(f"{label} is not a sideset name, or sides")
        return sides

    def get_nodes(self, label):
        nodes = None
        if isinstance(label, string_types):
            nodes = self.nodesets.get(label)
        elif isinstance(label, scalar_types):
            nodes = np.array([label], dtype=int)
        elif is_sequence(label):
            nodes = np.array(label, dtype=int)
        if nodes is None:
            raise ValueError(f"{label} is not a nodeset name, node, or nodes")
        return nodes

    def body_force(self, label, components):
        type, side_no = fetk.dload.body_force, -1
        elements = self.get_elems(label)
        values = fill_missing(components, length=3)
        for elem_no in elements:
            self.dloads.set(type, elem_no, side_no, values)

    def gravity(self, label, components):
        type, side_no = fetk.dload.body_force, -1
        elements = self.get_elems(label)
        f = fill_missing(components, length=3)
        for elem_no in elements:
            block_no = self.elem_map[elem_no][1]
            density = self.blocks[block_no].material.density
            self.dloads.set(type, elem_no, side_no, density * f)

    def surface_force(self, label, components):
        type = fetk.dload.surface_force
        sideset = self.get_sides(label)
        values = fill_missing(components, length=3)
        for side in sideset:
            elem_no, side_no = side
            self.dloads.set(type, elem_no, side_no - 1, values)

    def heat_source(self, label, magnitude):
        type, side_no = fetk.dload.body_flux, -1
        elements = self.get_elems(label)
        for elem_no in elements:
            self.dloads.set(type, elem_no, side_no, [magnitude, 0.0, 0.0])

    def surface_flux(self, label, components):
        type = fetk.dload.surface_flux
        values = fill_missing(components, length=3)
        sideset = self.get_sides(label)
        for side in sideset:
            elem_no, side_no = side
            self.dloads.set(type, elem_no, side_no - 1, values)

    def surface_film(self, label, Too, h):
        type = fetk.dload.surface_film
        sideset = self.get_sides(label)
        for side in sideset:
            elem_no, side_no = side
            self.dloads.set(type, elem_no, side_no - 1, [Too, h, 0.0])

    def pressure(self, label, amplitude):
        type = fetk.dload.pressure
        sideset = self.get_sides(label)
        for side in sideset:
            elem_no, side_no = side
            self.dloads.set(type, elem_no, side_no - 1, [-amplitude])

    def boundary(self, label, dofs, magnitude, type=fetk.boundary.dirichlet):
        """Prescribe boundary conditions at nodes."""
        nodes = self.get_nodes(label)
        dofs = self.get_dofs(dofs)
        for node_no in nodes:
            n = self.node_map.get(node_no)
            if n is None:
                raise ValueError(f"{node_no} is not a valid node ID")
            self.bcs.set(type, n, dofs, magnitude)

    def temperature(self, label, magnitude):
        """Prescribe temperature at nodes."""
        return self.boundary(label, "T", magnitude)

    def point_force(self, label, dofs, magnitude):
        return self.boundary(label, dofs, magnitude, type=fetk.boundary.neumann)

    def cload(self, label, dofs, magnitude):
        return self.boundary(label, dofs, magnitude, type=fetk.boundary.neumann)

    def dload(self, type, elem_no, side_no, *values):
        self.dloads.set(type, elem_no, side_no, values)

    # aliases
    surface_convection = surface_film
    film = surface_film
    concentrated_load = cload
    prescribed_displacement = boundary

    # geometry
    def find_nodes(self, key):
        node_map = sorted(self.node_map, key=lambda x: self.node_map[x])
        ix = np.where(key(self.coord))[0]
        return [node_map[i] for i in ix]

    def find_sides2d(self, key):
        sides = []
        start = 0
        elem_map = sorted(self.elem_map, key=lambda x: self.elem_map[x][0])
        nodes = np.where(key(self.coord))[0]
        for block in self.blocks:
            for (e, c) in enumerate(block.connect, start=start):
                w = np.where(np.in1d(c, nodes))[0]
                if len(w) < 2:
                    continue
                w = tuple(sorted(w))
                for (d, edge) in enumerate(block.element.edges):
                    if tuple(sorted(edge)) == w:
                        # the internal node numbers match, this is the edge
                        side = [elem_map[e], d + 1]
                        sides.append(side)
            start += len(block.connect)
        return np.array(sides)
