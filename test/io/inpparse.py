import numpy as np
from io import StringIO
from fetk.io.inpparse import parse_file


def xxxx_inpparse_truss():
    inp = """\
input:
  node:
  - 1, 0.00, 0.00
  - 2, 10.00, 5.00
  - 3, 10.00, 0.00
  - 4, 20.00, 8.00
  - 5, 20.00, 0.00
  - 6, 30.00, 9.00
  - 7, 30.00, 0.00
  - 8, 40.00, 8.00
  - 9, 40.00, 0.00
  - 10, 50.00, 5.00
  - 11, 50.00, 0.00
  - 12, 60.00, 0.00
  element:
  - 1, 1, 3
  - 2, 3, 5
  - 3, 5, 7
  - 4, 7, 9
  - 5, 9, 11
  - 6, 11, 12
  - 7, 1, 2
  - 8, 2, 4
  - 9, 4, 6
  - 10, 6, 8
  - 11, 8, 10
  - 12, 10, 12
  - 13, 2, 3
  - 14, 4, 5
  - 15, 6, 7
  - 16, 8, 9
  - 17, 10, 11
  - 18, 2, 5
  - 19, 4, 7
  - 20, 7, 8
  - 21, 9, 10
  element blocks:
  - block:
      material: 1
      element type: L2D2
      element properties:
        area: 1.
      elements: 1, 2, 3, 4, 5, 6
  - block:
      material: 2
      element type: L2D2
      element properties:
        area: 2.
      elements: 7, 8, 9, 10, 11, 12
  - block:
      material: 3
      element type: L2D2
      element properties:
        area: 3.
      elements: 13, 14, 15, 16, 17
  - block:
      material: 4
      element type: L2D2
      element properties:
        area: 4.
      elements: 18, 19, 20, 21
  materials:
  - material:
      id: 1
      model: elastic
      parameters:
        young's modulus: 1.
  - material:
      id: 2
      model: elastic
      parameters:
        young's modulus: 2.
  - material:
      id: 3
      model: elastic
      parameters:
        young's modulus: 3.
  - material:
      id: 4
      model: elastic
      parameters:
        young's modulus: 4.
  boundary:
  - 1, x, 0.00
  - 1, y, 0.00
  - 12, y, 0.00
  cload:
  - 3, y, -10.00
  - 5, y, -10.00
  - 7, y, -16.00
  - 9, y, -10.00
  - 11, y, -10.00
  dload:
  - 2, bx, 10
  - 3, by, 10.
  - 4, bz, 10.
  - 5, h, 10.
  - 2, px, 10
  - 3, py, 10.
  - 4, pz, 10.
  - 5, p, 10.
"""
    file = StringIO(inp)
    ns = parse_file(file)
    assert ns.jobid == "Job-1"
    assert ns.nodes[0] == [1, 0.0, 0.0]
    assert ns.nodes[-1] == [12, 60.0, 0.0]
    assert ns.elements[0] == [1, 1, 3]
    assert ns.elements[-1] == [21, 9, 10]
    assert ns.boundary[0] == (1, 0, 0.0)
    assert ns.cload[-1] == (11, 1, -10.0)
    assert ns.dload[-3] == (5, 0, 10.0)
    assert ns.dload[-2] == (5, 1, 10.0)
    assert ns.dload[-1] == (5, 2, 10.0)


def test_inpparse_dload():
    inp = """\
input:
  node:
  - 1, 0.00, 0.00
  - 2, 1.00, 0.00
  - 3, 1.00, 1.00
  - 4, 0.00, 1.00
  element:
  - 1, 1, 2, 3, 4
  - 2, 1, 2, 3, 4
  - 5, 1, 2, 3, 4
  - 8, 1, 2, 3, 4
  - 9, 1, 2, 3, 4
  - 10, 1, 2, 3, 4
  - 11, 1, 2, 3, 4
  - 12, 1, 2, 3, 4
  element blocks:
  - block:
      material: 1
      element type: C2D4S
      element properties:
        thickness: 1.
      elements: 1
  materials:
  - material:
      id: 1
      model: elastic
      parameters:
        young's modulus: 1.
  boundary:
  - 1, x, 0.00
  dload:
  - 2, bf, 6., 7., 8.
  - 5, p, 9.
  - 8, f2, 1., 2., 3.
  - 9, bx, 1.
  - 9, by, 2.
  - 9, bz, 3.
  dflux:
  - 10, bf, 3.
  - 11, f2, 4., 1.
  film:
  - 12, f3, 2., 3.
"""
    file = StringIO(inp)
    ns = parse_file(file)
    tags = [load[:3] for load in ns.dload]
    assert np.allclose(
        tags,
        [
            [10, 2, -1],
            [11, 5, -1],
            [12, 8, 2],
            [10, 9, -1],
            [10, 9, -1],
            [10, 9, -1],
            [20, 10, -1],
            [21, 11, 2],
            [22, 12, 3],
        ],
    )
    vals = [load[-1] for load in ns.dload]
    assert np.allclose(
        vals,
        [
            [6, 7, 8],
            [9, 0, 0],
            [1, 2, 3],
            [1, 0, 0],
            [0, 2, 0],
            [0, 0, 3],
            [3, 0, 0],
            [4, 1, 0],
            [2, 3, 0],
        ],
    )


def test_inpparse_boundary():
    inp = """\
input:
  node:
  - 1, 0.00, 0.00
  - 2, 1.00, 0.00
  - 3, 1.00, 1.00
  - 4, 0.00, 1.00
  element:
  - 1, 1, 2, 3, 4
  element blocks:
  - block:
      material: 1
      element type: C2D4S
      element properties:
        thickness: 1.
      elements: 1
  materials:
  - material:
      id: 1
      model: elastic
      parameters:
        young's modulus: 1.
  boundary:
  - 1, x, 1.00
  - 1, y, 2.00
  - 1, z, 3.00
  - 1, z, 5.00
  - 2, xy, 1.0
  - 3, xz, 1.0
  - 4, t, 1.0
"""
    file = StringIO(inp)
    ns = parse_file(file)
    assert ns.boundary == [
        [1, 1, "x", 1.0],
        [1, 1, "y", 2.0],
        [1, 1, "z", 3.0],
        [1, 1, "z", 5.0],
        [1, 2, "xy", 1.0],
        [1, 3, "xz", 1.0],
        [1, 4, "t", 1.0],
    ]
