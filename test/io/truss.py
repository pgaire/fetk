import numpy as np
import fetk.io.truss as truss_io
from fetk.util.filesystem import working_dir


def test_plot_truss_basic(tmpdir):
    with working_dir(tmpdir):
        coords = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.5, 0.5, 0.0]])
        connect = np.array([[0, 1], [0, 2], [1, 2]])
        truss_io.plot(coords, connect, filename="truss_test.pdf")


def test_plot_truss_overlay(tmpdir):
    with working_dir(tmpdir):
        coords = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.5, 0.5, 0.0]])
        connect = np.array([[0, 1], [0, 2], [1, 2]])
        overlay_coords = np.array(coords)
        overlay_coords[2, 1] += 0.1
        truss_io.plot(
            coords, connect, overlay_coords=overlay_coords, filename="truss_test.pdf"
        )
