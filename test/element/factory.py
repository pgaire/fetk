import fetk.element
import fetk.material


def test_element_factory():
    properties = {"area": 1.0}
    element = fetk.element.factory("L1D2", **properties)
    assert isinstance(element, fetk.element.L1D2)
