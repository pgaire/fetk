import numpy as np
import fetk.element
import fetk.material


def test_2d_quad():
    """
    4................ 2a .................3
    |                                     |
    |                                     |
    a                                     a
    |                                     |
    |                                     |
    1................ 2a .................2

    """
    properties = {"thickness": 1.0}
    a = 1.0
    xc = np.array([[0, 0], [2 * a, 0], [2 * a, a], [0, a]])
    material = fetk.material.elastic({"E": 96.0, "nu": 1.0 / 3.0})
    element = fetk.element.C2D4S(**properties)
    ke = element.stiffness(xc, material)
    expected = np.array(
        [
            [42, 18, -6, 0, -21, -18, -15, 0],
            [18, 78, 0, 30, -18, -39, 0, -69],
            [-6, 0, 42, -18, -15, 0, -21, 18],
            [0, 30, -18, 78, 0, -69, 18, -39],
            [-21, -18, -15, 0, 42, 18, -6, 0],
            [-18, -39, 0, -69, 18, 78, 0, 30],
            [-15, 0, -21, 18, -6, 0, 42, -18],
            [0, -69, 18, -39, 0, 30, -18, 78],
        ],
        dtype=float,
    )
    assert np.allclose(ke, expected)
    a = 2 * a * a
    assert np.allclose(element.area(xc), a)
