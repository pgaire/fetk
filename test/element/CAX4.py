import numpy as np
import fetk.element
import fetk.material
import fetk.dload

"""
z
^
|
|       4................ a ..................3
|       |                                     |
|       |                                     |
|       a                 1                   b
|       |                                     |
|       |                                     |
|.. d ..1................ a ..................2
|
--------------------------------------------------> r

"""

def test_axisymmetric_stiff():
    a, b, d = 4, 2, 0
    xc = np.array([[d, 0], [d + a, 0], [d + a, b], [d, b]])
    material = fetk.material.elastic({"E": 96.0, "nu": 1.0 / 3.0})
    element = fetk.element.CAX4()
    ke = element.stiffness(xc, material)
    expected = np.array(
        [
            [168, -12, 24, 12, -24, -36, 48, 36],
            [-12, 108, -24, 84, -72, -102, -36, -90],
            [24, -24, 216, -120, 0, 72, -24, 72],
            [12, 84, -120, 300, -72, -282, 36, -102],
            [-24, -72, 0, -72, 216, 120, 24, 24],
            [-36, -102, 72, -282, 120, 300, -12, 84],
            [48, -36, -24, 36, 24, -12, 168, 12],
            [36, -90, 72, -102, 24, 84, 12, 108],
        ],
        dtype=float,
    )
    assert np.allclose(ke, expected)
    area = a * b
    assert np.allclose(element.area(xc), area)


def test_axisymmetric_body_force_1():
    a, b, d = 6, 2, 1
    xc = np.array([[d, 0], [d + a, 0], [d + a, b], [d, b]])
    element = fetk.element.CAX4()
    dload = np.array([3, -1, 3, -1, 3, -1, 3, -1], dtype=float)
    fe = element.body_force(xc, dload)
    expected = np.array([27, -9, 45, -15, 45, -15, 27, -9], dtype=float)
    assert np.allclose(fe, expected)


def test_axisymmetric_body_force_2():
    a, b, d = 6, 2, 1
    xc = np.array([[d, 0], [d + a, 0], [d + a, b], [d, b]])
    element = fetk.element.CAX4()
    dload = np.array([d, 0, a, 0, a, 0, d, 0], dtype=float)
    fe = element.body_force(xc, dload)
    expected = np.array([29, 0, 70, 0, 70, 0, 29, 0], dtype=float)
    assert np.allclose(fe, expected)


def test_axisymmetric_force_1():
    a, b, d = 6, 2, 1
    xc = np.array([[d, 0], [d + a, 0], [d + a, b], [d, b]])
    element = fetk.element.CAX4()
    values = [np.array([3, -1, 3, -1, 3, -1, 3, -1], dtype=float)]
    tags = [[fetk.dload.body_force, -1]]
    dload = fetk.dload.dload(tags=tags, values=values)
    fe = element.force(xc, dload)
    expected = np.array([27, -9, 45, -15, 45, -15, 27, -9], dtype=float)
    assert np.allclose(fe, expected)


def test_axisymmetric_force_2():
    a, b, d = 6, 2, 1
    xc = np.array([[d, 0], [d + a, 0], [d + a, b], [d, b]])
    element = fetk.element.CAX4()
    values = [np.array([d, 0, a, 0, a, 0, d, 0], dtype=float)]
    tags = [[fetk.dload.body_force, -1]]
    dload = fetk.dload.dload(tags=tags, values=values)
    fe = element.force(xc, dload)
    expected = np.array([29, 0, 70, 0, 70, 0, 29, 0], dtype=float)
    assert np.allclose(fe, expected)
