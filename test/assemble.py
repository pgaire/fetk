import numpy as np
import fetk.element
import fetk.assemble
from fetk.material import elastic


def test_simple_assemble_2d():
    material = elastic({"E": 100.0})
    coords = np.array([[0.0, 0.0, 0.0], [10.0, 0.0, 0.0], [10.0, 10.0, 0.0]])
    blocks = [
        fetk.element.block.factory(
            1, [0], [[0, 1]], fetk.element.L3D2(area=1.0), material
        ),
        fetk.element.block.factory(
            2, [1], [[1, 2]], fetk.element.L3D2(area=0.5), material
        ),
        fetk.element.block.factory(
            3, [2], [[0, 2]], fetk.element.L3D2(area=2.0 * np.sqrt(2.0)), material
        ),
    ]
    K = fetk.assemble.stiffness(coords, blocks)
    expected = np.array(
        [
            [20.0, 10.0, 0.0, -10.0, 0.0, 0.0, -10.0, -10.0, 0.0],
            [10.0, 10.0, 0.0, 0.0, 0.0, 0.0, -10.0, -10.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [-10.0, 0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 5.0, 0.0, 0.0, -5.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [-10.0, -10.0, 0.0, 0.0, 0.0, 0.0, 10.0, 10.0, 0.0],
            [-10.0, -10.0, 0.0, 0.0, -5.0, 0.0, 10.0, 15.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        ]
    )
    assert np.allclose(K, expected)

    v = np.linalg.eigvals(K)
    v_expected = np.array([45.3577, 16.74031, 7.902, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    assert np.allclose(v, v_expected)
